# README #

### This repository is KICAD project for minimal microcontroller boards with pads for components to each io pin  ###

* Only few components are needed
* Even Crystal/Ceramic resonator is optional
* More detailed information about the board(s) can be found from the [wiki](https://bitbucket.org/kaprot/mcuxproto/wiki/MEGAX8%20PROTO)

### How do I get set up? ###

* Clone or download the project
* Start kicad